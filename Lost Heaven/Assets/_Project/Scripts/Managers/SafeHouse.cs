using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Managers;

public class SafeHouse : MonoBehaviour
{
    public bool m_playerInside;
    public FlashlightSTO m_flashlight;
    [SerializeField] private float m_refillRate = 5f;
    [SerializeField] private int m_refillQuantity = 5;
    [SerializeField] private GameEventSTO m_OnDrain;

    // Start is called before the first frame update
    void Start()
    {
        m_playerInside = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.PlayerInSafeHouse = true;
            m_playerInside = true;
            RechargeBateries(m_playerInside);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!m_playerInside)
        {
            if (other.tag == "Player")
            {
                GameManager.Instance.PlayerInSafeHouse = true;
                m_playerInside = true;
                RechargeBateries(m_playerInside);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.PlayerInSafeHouse = false;
            m_playerInside = false;
            RechargeBateries(m_playerInside);
        }
    }

    public void RechargeBateries(bool start)
    {
        if (start)
        {
            StartCoroutine(Recharge());
        }
        else
        {
            StopAllCoroutines();
        }
    }
    
    IEnumerator Recharge()
    {
        while (m_playerInside)
        {
            if (!m_flashlight.IsFull)
            {
                m_flashlight.RefillBattery(m_refillQuantity);
                m_OnDrain.Raise();
            }
            yield return new WaitForSeconds(m_refillRate);
        }
    }
}
