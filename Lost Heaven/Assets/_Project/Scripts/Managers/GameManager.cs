using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        [SerializeField] private bool m_paused;
        [SerializeField] private bool m_playerInSafeHouse = false;
        [SerializeField] private bool m_hasBeenLoaded = false;
        [SerializeField] private bool m_loadAtStart = true;

        public bool Paused { get => m_paused; set => m_paused = value; }
        public bool PlayerInSafeHouse { get => m_playerInSafeHouse; set => m_playerInSafeHouse = value; }
        public bool HasBeenLoaded { get => m_hasBeenLoaded; set => m_hasBeenLoaded = value; }
        public bool LoadAtStart { get => m_loadAtStart; set => m_loadAtStart = value; }


        // Start is called before the first frame update
        void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            StartCoroutine(LoadingGame());
        }

        IEnumerator LoadingGame()
        {
            yield return new WaitForSeconds(4f);
            m_hasBeenLoaded = true;
        }
    }

}
