using UnityEngine.SceneManagement;
using UnityEngine;
using Michsky.LSS;

public class FastTravel : MonoBehaviour
{
    [SerializeField] private LoadingScreenManager m_lsm;
    [SerializeField] private CharacterSTO m_player;
    private Vector3 m_safeHouse = new Vector3(538f, 24f, 656f);
    private Vector3 m_raido = new Vector3(989f, 84f, 965f);
    private Vector3 m_wunjo = new Vector3(273f, 39.2f, 718f);
    private Vector3 m_jera = new Vector3(774f, 37.5f, 136f);
    private Vector3 m_aelghiz = new Vector3(592f, 36f, 986f);
    private Vector3 m_thurizas = new Vector3(943f, 70.5f, 367f);
    private Vector3 m_teiwaz = new Vector3(324f, 38.5f, 185f);

    void Start()
    {
        
    }

    public void FastTravelTo(int index)
    {
        Vector3 destiny = new Vector3();
        switch (index)
        {
            case 0:
                destiny = m_safeHouse;
                break;
            case 1:
                destiny = m_raido;
                break;
            case 2:
                destiny = m_wunjo;
                break;
            case 3:
                destiny = m_jera;
                break;
            case 4:
                destiny = m_aelghiz;
                break;
            case 5:
                destiny = m_thurizas;
                break;
            case 6:
                destiny = m_teiwaz;
                break;
        }
        m_player.CurrentPosition = destiny;
        m_lsm.LoadScene(SceneManager.GetActiveScene().name);
    }
    
}
