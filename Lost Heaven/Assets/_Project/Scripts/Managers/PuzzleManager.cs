using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    [SerializeField] private PuzzleInventorySTO m_inventory;
    [SerializeField] private GameObject[] m_keys;
    [SerializeField] private PuzzleSTO[] m_puzzles;
    

    public void KeyTaken(string keyname)
    {
        switch (keyname)
        {
            case "Shield":
                m_inventory.HasKey[0] = false;
                m_inventory.HasKey[1] = true;
                m_inventory.HasKey[2] = false;
                m_inventory.HasKey[3] = false;
                break;
            case "Skull":
                m_inventory.HasKey[2] = true;
                m_inventory.HasKey[0] = false;
                m_inventory.HasKey[1] = false;
                m_inventory.HasKey[3] = false;
                break;
            case "Key":
                m_inventory.HasKey[3] = true;
                m_inventory.HasKey[1] = false;
                m_inventory.HasKey[0] = false;
                m_inventory.HasKey[2] = false;
                break;
        }
    }
    
}
