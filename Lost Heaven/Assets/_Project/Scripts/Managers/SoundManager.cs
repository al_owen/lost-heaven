using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    [SerializeField] private AudioSource m_audioSource;

    [TabGroup("Hud")]
    [AssetList(Path = "/_Project/Resources/STOs/Audio/Hud SFX", AutoPopulate =true)]
    public List<SfxSTO> Hud;

    [TabGroup("Animals")]
    [AssetList(Path = "/_Project/Resources/STOs/Audio/Animals SFX", AutoPopulate = true)]
    public List<SfxSTO> Animals;

    [TabGroup("Noises")]
    [AssetList(Path = "/_Project/Resources/STOs/Audio/Noises SFX", AutoPopulate = true)]
    public List<SfxSTO> Noises;

    [TabGroup("BasicSounds")]
    [AssetList(Path = "/_Project/Resources/STOs/Audio/BasicSounds SFX", AutoPopulate = true)]
    public List<SfxSTO> BasicSounds;

    [TabGroup("Runes")]
    [AssetList(Path = "/_Project/Resources/STOs/Audio/Runes SFX", AutoPopulate = true)]
    public List<SfxSTO> Runes;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            m_audioSource = this.gameObject.GetComponent<AudioSource>();
        }
    }

    public static void PlaySFX(SfxSTO clip, bool waitToFinish = true, AudioSource audioSource = null)
    {
        if(audioSource == null)
        {
            audioSource = Instance.m_audioSource;
        }

        if (audioSource == null)
        {
            print("There's no audio source");
        }


        if(!audioSource.isPlaying || !waitToFinish)
        {
            audioSource.clip = clip.Sound;
            audioSource.volume = clip.Volume + Random.Range(-clip.VolumeVariation, clip.VolumeVariation);
            audioSource.pitch = clip.pitch + Random.Range(-clip.pitchVariation, clip.pitchVariation);
            audioSource.Play();
            //print(clip.name +" is playing");
        }
    }

    [HorizontalGroup("AudioSource")]
    [ShowIf("@m_audioSource == null")]
    [Button]
    private void AddAudioSource()
    {
        m_audioSource = this.gameObject.GetComponent<AudioSource>();

        if (m_audioSource == null)
        {
            m_audioSource = this.gameObject.AddComponent<AudioSource>();
        }
    }
}
