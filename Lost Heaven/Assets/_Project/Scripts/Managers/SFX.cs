using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class SFX
{
    [LabelText("SFX Type")]
    [LabelWidth(100)]
    [OnValueChanged("SFXChange")]
    [InlineButton("PlaySFX")]
    public SFXType sfxType = SFXType.Hud;

    [LabelText("$sfxLabel")]
    [LabelWidth(100)]
    [ValueDropdown("SFXType")]
    [OnValueChanged("SFXChange")]
    [InlineButton("SelectSFX")]
    public SfxSTO sfxToPlay;
    private string sfxLabel = "SFX";

#pragma warning disable 0414
    [SerializeField]
    private bool showSettings = false;
    [SerializeField]
    private bool editSettings = false;
#pragma warning restore 0414


    [InlineEditor(InlineEditorObjectFieldModes.Hidden)]
    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    private SfxSTO _sfxBase;

    [Title("Audio Source")]
    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private bool waitToPlay = true;

    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private bool useDefault = true;

    [DisableIf("useDefault")]
    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private AudioSource audioSource;


    private void SFXChange()
    {
        sfxLabel = sfxType.ToString() + " SFX";
        _sfxBase = sfxToPlay;
    }

    private void SelectSFX()
    {
#if UNITY_EDITOR
        Selection.activeObject = sfxToPlay;
#endif
    }

    private List<SfxSTO> getSFXType()
    {
        List<SfxSTO> sfxList;

        switch (sfxType)
        {
            case SFXType.Hud:
                sfxList = SoundManager.Instance.Hud;
                break;
            case SFXType.Animals:
                sfxList = SoundManager.Instance.Animals;
                break;
            case SFXType.Noises:
                sfxList = SoundManager.Instance.Noises;
                break;
            case SFXType.BasicSounds:
                sfxList = SoundManager.Instance.BasicSounds;
                break;
            case SFXType.Runes:
                sfxList = SoundManager.Instance.Runes;
                break;
            default:
                sfxList = SoundManager.Instance.Hud;
                break;
        }
        return sfxList;
    }

    public void PlaySFX()
    {
        if(useDefault || audioSource == null)
        {
            SoundManager.PlaySFX(sfxToPlay, waitToPlay, null);
        }
        else
        {
            SoundManager.PlaySFX(sfxToPlay, waitToPlay, audioSource);
        }
    }
}
public enum SFXType
{
    Noises, Hud, Animals, BasicSounds, Runes
}