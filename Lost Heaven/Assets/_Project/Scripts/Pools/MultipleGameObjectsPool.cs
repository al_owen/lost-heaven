﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// pool for multiple kinds of gameobjects
/// </summary>
public class MultipleGameObjectsPool : MonoBehaviour
{
    [SerializeField] private string _name;
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private int _initialQuantity;

    public static MultipleGameObjectsPool Instance { get; private set; }
    public string Name { get => _name; private set => _name = value; }
    private Queue<GameObject> objects = new Queue<GameObject>();

    private void Awake()
    {
        Instance = this;
        addObjects(_initialQuantity);
    }

    public GameObject Get()
    {
        if (objects.Count == 0)
        {
            addObjects(1);
        }
        return objects.Dequeue();
    }

    public void returnToPool(GameObject objectToReturn)
    {
        objectToReturn.SetActive(false);
        objects.Enqueue(objectToReturn);
    }

    private void addObjects(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject newObject = Instantiate(prefabs[Random.Range(0, prefabs.Length)]);
            newObject.SetActive(false);
            objects.Enqueue(newObject);
            newObject.transform.SetParent(this.transform);
            newObject.GetComponent<IMultipleGameObjectPooled>().Pool = this;
        }
    }
}

/// <summary>
/// interface to implement pool
/// </summary>
public interface IMultipleGameObjectPooled
{
    MultipleGameObjectsPool Pool { get; set; }
}
