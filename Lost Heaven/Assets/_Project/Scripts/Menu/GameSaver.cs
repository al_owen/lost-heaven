using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine;
using Michsky.LSS;
using Managers;

public class GameSaver : MonoBehaviour, ISaveableCharacter
{
    [SerializeField] private LoadingScreenManager m_lss;
    [SerializeField] private CharacterSTO m_player;
    [SerializeField] private PuzzleInventorySTO m_inventory;
    [SerializeField] private FlashlightSTO m_flashlight;
    [SerializeField] private PuzzleSTO[] m_puzzles;
    private FirstPersonController m_playerObject;
    private GameObject m_totem;
    [SerializeField] private bool m_restartPuzzles = false;

    private bool restarting = false;
    public bool Restarting { get => restarting; set => restarting = value; }

    private void Start()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName != "Menu" && sceneName !="HighlightPuzzle") { m_playerObject = GameObject.Find("Player").GetComponent<FirstPersonController>(); }
        
        m_totem = GameObject.Find("Protection");
        if (GameManager.Instance.LoadAtStart)
        { 
            LoadGame(true);
            GameManager.Instance.LoadAtStart = false;
        }
    }


    public void SaveGame()
    {
        SavePlayerData sd = new SavePlayerData();
        PopulateSaveData(sd);
        if (FileManager.WriteToFile("Character.json", sd.ToJson()))
        {
            Debug.Log("Save Succesful!");
        }
    }

    public void LoadGame(bool loadScene = true)
    {
        if (FileManager.LoadFromFile("Character.json", out var json))
        {
            SavePlayerData sd = new SavePlayerData();
            sd.LoadFromJson(json);
            LoadFromSaveData(sd);
            Debug.Log("Load complete!");
            Time.timeScale = 1f;
            if (loadScene) { m_lss.LoadScene(sd.SceneName); }
        }
    }


    public void LoadFromSaveData(SavePlayerData saveData)
    {
        m_player.CurrentPosition = saveData.CurrentPosition;
        m_player.ActivePosition = saveData.ActivePosition;
        m_player.CorruptedLevel = saveData.CorruptedLevel;
        m_player.FullCorruption = saveData.FullCorruption;
        m_player.CorruptionLevel = saveData.CorruptionLevel;
        m_player.RunningSpeed = saveData.RunningSpeed;
        m_player.WalkingSpeed = saveData.WalkingSpeed;
        m_inventory.HasKey = saveData.KeysInInventory;
        m_inventory.UsedKeys = saveData.KeysUsed;
        //flashlight
        m_flashlight.MaxBattery = saveData.MaxBattery;
        m_flashlight.CurrentBattery = saveData.CurrentBattery;
        m_flashlight.CurrentMode = saveData.CurrentMode;
        m_flashlight.HalfIntensity = saveData.HalfIntensity;
        m_flashlight.FullIntensity = saveData.FullIntensity;
        m_flashlight.IsEmpty = saveData.IsEmpty;
        m_flashlight.IsFull = saveData.IsFull;
        m_flashlight.NeedRecharge = saveData.NeedRecharge;
        m_flashlight.BatteriesLeft = saveData.BatteriesLeft;
        m_flashlight.MaxBatteries = saveData.MaxBatteries;
        //ability
        m_flashlight.CurrentAbility = saveData.CurrentAbility;
        
    }

    public void PopulateSaveData(SavePlayerData saveData)
    {
        if(!restarting)
        {
            saveData.SceneName = SceneManager.GetActiveScene().name;
            m_playerObject.SetPosition();
        }
        else
        {
            saveData.SceneName = "Forest";
        }
        saveData.CurrentPosition = m_player.CurrentPosition;
        saveData.ActivePosition = m_player.ActivePosition;
        saveData.CorruptedLevel = m_player.CorruptedLevel;
        saveData.FullCorruption = m_player.FullCorruption;
        saveData.CorruptionLevel = m_player.CorruptionLevel;
        saveData.RunningSpeed = m_player.RunningSpeed;
        saveData.WalkingSpeed = m_player.WalkingSpeed;
        saveData.KeysInInventory = m_inventory.HasKey;
        saveData.KeysUsed = m_inventory.UsedKeys;
        //flashlight
        saveData.MaxBattery = m_flashlight.MaxBattery;
        saveData.CurrentBattery = m_flashlight.CurrentBattery;
        saveData.CurrentMode = m_flashlight.CurrentMode;
        saveData.HalfIntensity = m_flashlight.HalfIntensity;
        saveData.FullIntensity = m_flashlight.FullIntensity;
        saveData.IsEmpty = m_flashlight.IsEmpty;
        saveData.IsFull = m_flashlight.IsFull;
        saveData.NeedRecharge = m_flashlight.NeedRecharge;
        saveData.BatteriesLeft = m_flashlight.BatteriesLeft;
        saveData.MaxBatteries = m_flashlight.MaxBatteries;
        //ability data
        saveData.CurrentAbility = m_flashlight.CurrentAbility;
    }

    /// <summary>
    /// Restart values and saves
    /// </summary>
    public void RestartGame()
    {
        restarting = true;
        m_player.CurrentPosition = new Vector3(538f, 24f, 656f);
        m_player.ActivePosition = true;
        m_player.CorruptedLevel = 0;
        m_player.FullCorruption = 100;
        m_player.CorruptionLevel = CorruptionLevel.NotCorrupted;
        m_player.RunningSpeed = 10f;
        m_player.WalkingSpeed = 5f;
        m_inventory.HasKey = new bool[4];
        m_inventory.UsedKeys = new bool[4];
        //flashlight
        m_flashlight.MaxBattery = 100;
        m_flashlight.CurrentBattery = 100;
        m_flashlight.CurrentMode = LightMode.off;
        m_flashlight.HalfIntensity = 20000f;
        m_flashlight.FullIntensity = 40000f;
        m_flashlight.IsEmpty = false;
        m_flashlight.IsFull = true;
        m_flashlight.NeedRecharge = false;
        m_flashlight.BatteriesLeft = 10;
        m_flashlight.MaxBatteries = 10;
        //ability
        m_flashlight.CurrentAbility = null;
        SaveGame();
        m_restartPuzzles = true;
        RestartPuzzles();
    }

    private void RestartPuzzles()
    {
        if (m_restartPuzzles)
        {
            foreach (PuzzleSTO puzzle in m_puzzles)
            {
                puzzle.ActivePuzzle = false;
                puzzle.FinishedPuzzle = false;
                puzzle.RewardTaken = false;
                for (int i = 0; i < puzzle.ActiveElements.Length; i++)
                {
                    puzzle.ActiveElements[i] = false;
                }
            }
            for (int i = 0; i < m_inventory.HasKey.Length; i++)
            {
                m_inventory.HasKey[i] = false;
                m_inventory.UsedKeys[i] = false;
            }
        }

    }
}
