using System.Collections.Generic;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System;

public class SettingsMenu : MonoBehaviour, ISaveableSettings
{
    [SerializeField]private AudioMixer AudioMixer;
    [SerializeField]private Slider m_volumeSlider;
    [SerializeField]private Toggle m_fullscreenToggle;
    [SerializeField]private TMP_Dropdown m_qualityDropdown, m_resolutionDropdown;


    private string m_volumeName = "Volume";
    private float m_volume = 0f;
    private int m_qualityIndex = 2;
    private int m_resolutionIndex = 0;
    private bool m_isFullScreen = true;
    private Resolution[] m_resolutions;

    private void Awake()
    {
        initResolution();
        if(!File.Exists(Application.persistentDataPath + "/Settings.json"))
        {
            SaveSettings();
        }
        else
        {
            LoadSettings();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_fullscreenToggle = transform.Find("Settings").Find("FullScreen").GetComponent<Toggle>();
        m_volumeSlider = transform.Find("Settings").Find("Volume").Find("Volume Slider").GetComponent<Slider>();
        m_qualityDropdown = transform.Find("Settings").Find("Quality").Find("Dropdown").GetComponent<TMP_Dropdown>();
        m_resolutionDropdown = transform.Find("Settings").Find("Resolution").Find("Dropdown").GetComponent<TMP_Dropdown>();
        LoadSettings();
    }

    private void initResolution()
    {
        int currentResIndex = 0;
        m_resolutions = Screen.resolutions;
        m_resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        for (int i = 0; i < m_resolutions.Length; i++)
        {
            string option = m_resolutions[i].width + " x " + m_resolutions[i].height;
            options.Add(option);

            if(m_resolutions[i].width == Screen.currentResolution.width && m_resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        m_resolutionDropdown.AddOptions(options);
        m_resolutionDropdown.value = currentResIndex;
        m_resolutionDropdown.RefreshShownValue();
        //SaveSettings();
    }

    public void SetVolume(float volume)
    {
        AudioMixer.SetFloat(m_volumeName, volume);
        m_volume = volume;
        PlayerPrefs.SetFloat(m_volumeName, m_volume);
        PlayerPrefs.Save();
    }
    
    

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        m_qualityIndex = qualityIndex;
    }

    public void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
        m_isFullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution currentRes = m_resolutions[resolutionIndex];
        m_resolutionIndex = resolutionIndex;
        Screen.SetResolution(currentRes.width, currentRes.height, m_isFullScreen);
    }


    /*************************Save and load from Json*****************************/

    public void SaveSettings()
    {
        SaveSettings sd = new SaveSettings();
        PopulateSaveData(sd);
        if (FileManager.WriteToFile("Settings.json", sd.ToJson()))
        {
            Debug.Log("Save Succesful!");
        }
    }

    public void LoadSettings()
    {
        if(FileManager.LoadFromFile("Settings.json", out var json))
        {
            SaveSettings sd = new SaveSettings();
            sd.LoadFromJson(json);
            LoadFromSaveData(sd);
            //Debug.Log("Load complete!");
        }
    }

    public void PopulateSaveData(SaveSettings saveData)
    {
        saveData.Volume = m_volume;
        saveData.Quality = m_qualityIndex;
        saveData.Fullscreen = m_isFullScreen;
        saveData.Resolution = m_resolutionIndex;
    }

    public void LoadFromSaveData(SaveSettings saveData)
    {
        var loadVolume = saveData.Volume;
        m_volumeSlider.value = loadVolume;
        AudioMixer.SetFloat(m_volumeName, loadVolume);
        var quality = saveData.Quality;
        SetQuality(quality);
        m_qualityDropdown.value = quality;
        var resolution = saveData.Resolution;
        SetResolution(resolution);
        m_resolutionDropdown.value = resolution;
        var fullscreen = saveData.Fullscreen;
        SetFullscreen((fullscreen));
        m_fullscreenToggle.isOn = fullscreen;
    }
}
