#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Managers;

public class PauseMenu : MonoBehaviour
{
    private GameObject m_pause;
    private GameObject m_settings;
    private GameObject m_map;
   
    void Awake()
    {
        m_pause = transform.Find("PauseMenu").gameObject;
        m_settings = transform.Find("Settings").gameObject;
        m_map = transform.Find("Map").gameObject;
        
    }
    private void Start()
    {
        StartCoroutine(AfterLoad());
        
    }

    void Update()
    {
        if (!GameManager.Instance.HasBeenLoaded) { return; }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseMode();
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            OpenMap();
        }
    }

    private void PauseMode()
    {
        if (m_map.activeSelf){ OpenMap(); }
        
        if (GameManager.Instance.Paused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    private void EnableCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    private void DisableCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ResumeGame()
    {
        GameManager.Instance.Paused = false;
        DisableCursor();
        Time.timeScale = 1;
        m_pause.SetActive(false);
        if (m_settings.activeSelf) { m_settings.SetActive(false); }
    }

    public void PauseGame()
    {
        GameManager.Instance.Paused = true;
        EnableCursor();
        Time.timeScale = 0;
        m_pause.SetActive(true);
    }

    public void OpenSettings()
    {
        m_pause.SetActive(false);
        m_settings.SetActive(true);
    }

    public void closeSettings()
    {
        m_pause.SetActive(true);
        m_settings.SetActive(false);
    }

    public void OpenMap()
    {
        if (m_pause.activeSelf) { return; }

        if (m_map.activeSelf)
        {
            m_map.SetActive(false);
            DisableCursor();
            GameManager.Instance.Paused = false;
        }
        else
        {
            m_map.SetActive(true);
            EnableCursor();
            GameManager.Instance.Paused = true;
        }
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("Menu");
    }

    IEnumerator AfterLoad()
    {
        yield return new WaitForSeconds(4f);
        ResumeGame();
    }

}
