using UnityEngine;
using System.IO;
using System;

public static class FileManager 
{
    public static bool WriteToFile(string FileName, string FileContents)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, FileName);

        try
        {
            Debug.Log("Saved in "+fullPath);
            File.WriteAllText(fullPath, FileContents);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError($"failed to write to {fullPath} with exeception {e}");
        }
        return false;
    }

    public static bool LoadFromFile(string FileName, out string Result)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, FileName);

        try
        {
            Debug.Log("Loaded from "+fullPath);
            Result = File.ReadAllText(fullPath);
            return true;
        }
        catch(Exception e)
        {
            Debug.LogError($"failed to read to {fullPath} with exeception {e}");
            Result = "";
            return false;
        }
    }
}
