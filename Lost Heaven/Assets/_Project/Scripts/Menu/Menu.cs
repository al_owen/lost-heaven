﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine;
using Michsky.LSS;

public class Menu : MonoBehaviour
{
    private GameObject m_settings, m_menu, m_continueButton;
    [SerializeField] private AudioMixer audioMixer;
    private Camera m_camera;
    public LoadingScreenManager m_lsm;
    public GameSaver Saver;
    [SerializeField] private CharacterSTO m_player;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_camera = Camera.main;
        m_settings = this.gameObject.transform.Find("Settings").gameObject;
        m_menu = this.gameObject.transform.Find("Menu").gameObject;
        m_continueButton = m_menu.transform.Find("Continue").gameObject;
        if (!File.Exists(Application.persistentDataPath + "/Character.json"))
        {
            m_continueButton.SetActive(false);
        }
        else
        {
            m_continueButton.SetActive(true);
        }
    }

    /// <summary>
    /// starts a new game 
    /// </summary>
    public void NewGame()
    {
        Saver.RestartGame();
        Saver.LoadGame();
    }

    /// <summary>
    /// loads game
    /// </summary>
    public void ContinueGame()
    {
        if (m_player.CorruptionLevel == CorruptionLevel.FullyCorrupted) { Saver.RestartGame(); }
        Saver.LoadGame(true);
    }

    /// <summary>
    /// exits the game
    /// </summary>
    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    /// <summary>
    /// open the settings menu
    /// </summary>
    public void OpenSettings(){
        m_menu.SetActive(false);
        m_settings.SetActive(true);
    }

    /// <summary>
    /// closes the setting menu 
    /// </summary>
    public void CloseSettings(){
        m_settings.SetActive(false);
        m_menu.SetActive(true);
    }

    /// <summary>
    /// sets the volume
    /// </summary>
    /// <param name="volume">volume value</param>
    public void SetVolume(float volume){
        audioMixer.SetFloat("Volume", volume);
    }

    
}
