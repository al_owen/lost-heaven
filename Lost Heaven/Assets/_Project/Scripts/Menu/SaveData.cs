using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public string ToJson()
    {
        return JsonUtility.ToJson(this, true);
    }

    public void LoadFromJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }
}

public class SaveSettings : SaveData
{
    public float Volume = 0f;
    public int Quality = 2;
    public bool Fullscreen = true;
    public int Resolution = 0;
}

public class SavePlayerData : SaveData
{
    //Scenedata
    public string SceneName;
    //Player data
    public float WalkingSpeed;
    public float RunningSpeed;
    public float CorruptedLevel;
    public float FullCorruption;
    public CorruptionLevel CorruptionLevel;
    public Vector3 CurrentPosition;
    public bool ActivePosition;
    //inventory data
    public bool[] KeysInInventory;
    public bool[] KeysUsed;
    //flashlight data
    public int MaxBattery;
    public int CurrentBattery;
    public LightMode CurrentMode;
    public float HalfIntensity = 20000f;
    public float FullIntensity = 40000f;
    public bool IsEmpty;
    public bool IsFull;
    public bool NeedRecharge;
    public int BatteriesLeft;
    public int MaxBatteries = 10;
    public AbilitySTO CurrentAbility;
    
}

public class PuzzleData
{
    public int number = 0;
    public bool RewardTaken = false;
    public bool ActivePuzzle = false;
    public bool[] ActiveElements = new bool[1];
    public bool FinishedPuzzle = false;
}

public interface ISaveableSettings
{
    void PopulateSaveData(SaveSettings saveData);
    void LoadFromSaveData(SaveSettings saveData);
}

public interface ISaveableCharacter
{
    void PopulateSaveData(SavePlayerData saveData);
    void LoadFromSaveData(SavePlayerData saveData);
}

