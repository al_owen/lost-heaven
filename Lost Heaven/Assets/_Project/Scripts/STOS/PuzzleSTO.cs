using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "puzzle", menuName = "Puzzles/puzzle")]
[InlineEditor]
public class PuzzleSTO : ScriptableObject
{
    public bool RewardTaken;
    public bool ActivePuzzle;
    public bool[] ActiveElements;
    public bool FinishedPuzzle;
}
