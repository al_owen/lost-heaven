using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "FlashLight", menuName = "Objects/Flashlight")]
[InlineEditor]
public class FlashlightSTO : ScriptableObject
{
    public int MaxBattery;
    public int CurrentBattery;
    public LightMode CurrentMode;
    public float HalfIntensity = 20000f;
    public float FullIntensity = 40000f;
    public bool IsEmpty = false;
    public bool IsFull = false;
    public bool NeedRecharge;
    public int BatteriesLeft;
    public int MaxBatteries = 5;
    public AbilitySTO CurrentAbility;

    private void Awake()
    {
        if (CurrentBattery == MaxBatteries && BatteriesLeft == MaxBatteries)
        {
            IsFull = true;
        }
    }

    public void Drain()
    {
        if (IsEmpty || CurrentBattery == 0) { return; }
        IsFull = false;
        if (CurrentMode == LightMode.on)
        {
            CurrentBattery -= 1;
        }
        else if (CurrentMode == LightMode.full)
        {
            CurrentBattery -= 2;
        }

        if(CurrentBattery <= 0)
        {
            CurrentBattery = 0;
            if(BatteriesLeft == 0)
            {
                IsEmpty = true;
            }
        }
    }

    public void Recharge()
    {
        if(BatteriesLeft > 0)
        {
            BatteriesLeft--;
            CurrentBattery = MaxBattery;
        }

        if(BatteriesLeft <= 0)
        {
            BatteriesLeft = 0;
            //IsEmpty = true;
        }
    }

    public void RefillBattery(int refillQuantity)
    {
        if (CurrentBattery < MaxBattery)
        {
            CurrentBattery += refillQuantity;
            if (IsEmpty) { IsEmpty = false; }

            if (CurrentBattery >= MaxBattery)
            {
                CurrentBattery = MaxBattery;
                BatteriesLeft = MaxBatteries;
                IsFull = true;
            }
        }

        
    }
}
