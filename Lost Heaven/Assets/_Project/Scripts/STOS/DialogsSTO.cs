using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialog", menuName = "Character/Dialog")]
public class DialogsSTO : ScriptableObject
{
    public string[] DialogText;
    public AudioClip[] DialogSound;
}
