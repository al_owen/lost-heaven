using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
[InlineEditor]
public class PersistableSTO : ScriptableObject
{
    public Vector3 CurrentPosition;
    public Quaternion CurrentRotation;
    public bool Active;
    public bool HasBeenPlaced;
}
