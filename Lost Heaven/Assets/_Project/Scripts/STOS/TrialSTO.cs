using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Trial", menuName ="Puzzles/Trial")]
[InlineEditor]
public class TrialSTO : ScriptableObject
{
    [TextArea] public string Story;
    public SfxSTO NarratedStory;
    public bool Found;
}
