using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName ="Puzzle Inventory", menuName ="Puzzles/Puzzle Inventory")]
[InlineEditor]
public class PuzzleInventorySTO : ScriptableObject
{
    public bool[] HasKey, UsedKeys;
}
