using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Runes")]
public class RuneSTO : ScriptableObject
{
    public string RuneName;
    [PreviewField]
    public Sprite RuneImage;
    public DialogsSTO Dialog;

    public bool HasBeenFound;
}
