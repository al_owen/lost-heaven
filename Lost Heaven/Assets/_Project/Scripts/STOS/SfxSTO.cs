using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "Sounds/Effect")]
[InlineEditor]
public class SfxSTO : ScriptableObject
{
    [Header("List of sounds")]
    [Required]
    public AudioClip Sound;

    [Title("Settings for Clips")]
    [Range(0f, 1f)]
    public float Volume = 0.15f;
    [Range(0f, 0.2f)]
    public float VolumeVariation = 0.05f;
    [Range(0f, 2f)]
    public float pitch = 1f;
    [Range(0f, 0.2f)]
    public float pitchVariation = 0.05f;
}
