using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Base Character", menuName = "Character/Base Character")]
[InlineEditor]
public class CharacterSTO : ScriptableObject
{
    public float WalkingSpeed;
    public float RunningSpeed;
    public float CorruptedLevel = 0;
    public float FullCorruption = 100f;

    public CorruptionLevel CorruptionLevel;

    public Vector3 CurrentPosition;
    public bool ActivePosition;


}
