using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Enviromental Sounds", menuName = "Sounds/Enviromental Sounds")]
public class EnviromentalSoundsSTO : SoundsSTO
{
    [Title("Sound of the limits")]
    public AudioClip LimitSound;
}
