using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Character Sounds", menuName = "Sounds/Character Sounds")]
[InlineEditor]
public class CharacterSoundsSTO : SoundsSTO
{
    public AudioClip Pain;
    public AudioClip Grunt;
    public AudioClip HeavyBreathing;
    public AudioClip Scream;

}
