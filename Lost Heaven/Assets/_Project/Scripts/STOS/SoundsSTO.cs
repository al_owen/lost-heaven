using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class SoundsSTO : ScriptableObject
{
    [Header("List of sounds")]
    [Required]
    public List<AudioClip> Sounds;


    public AudioClip GetRandomSound()
    {
        int index = Random.Range(0, Sounds.Count);
        AudioClip sound = Sounds[index];
        return sound;
    }
}
