using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Ability", menuName = "Objects/Ability")]
[InlineEditor]
public class AbilitySTO : ScriptableObject
{
    public Abilities CurrentAbilities;    
    [PreviewField] public Sprite Sprite;
    public float Range;
    public float Time;
    public int Usage;
    public LayerMask Layers;
}
