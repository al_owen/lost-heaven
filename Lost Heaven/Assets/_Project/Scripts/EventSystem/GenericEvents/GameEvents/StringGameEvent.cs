using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object for game events without any parameters passed
/// </summary>
[CreateAssetMenu(fileName = "String Event", menuName = "Events/String Event")]
public class StringGameEvent : BaseGameEventSTO<string>
{
    
}
