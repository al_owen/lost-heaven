using UnityEngine.Events;
using UnityEngine;

public abstract class BaseGameEventListener<T, E, UER> : MonoBehaviour, IGameEventListener<T> where E : BaseGameEventSTO<T> where UER: UnityEvent<T>
{
    [Tooltip("Event to register with")]
    [SerializeField]private E gameEvent;

    [Tooltip("Response to invoke when Event is raised")]
    public UER Response;

    public E GameEvent { get => gameEvent; set => gameEvent = value; }

    private void OnEnable()
    {
        if (GameEvent == null) { return; }
        GameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        if (GameEvent == null) { return; }
        GameEvent.UnregisterListener(this);
    }

    public void OnEventRaised(T item)
    {
        if (Response == null) { return; }
        Response.Invoke(item);
    }
}
