﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// game event listener that does not expect any parameter
/// </summary>
public class GameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with")]
    public GameEventSTO GameEvent;

    [Tooltip("Response to invoke when Event is raised")]
    public UnityEvent Response;


    private void OnEnable()
    {
        if (GameEvent == null) { return; }
        GameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        if (GameEvent == null) { return; }
        GameEvent.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        if (Response == null) { return; }
        Response.Invoke();
    }

}
