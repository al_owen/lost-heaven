using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour
{

    private Transform m_playerTransform;
    private Vector3 m_dir;

    void Awake()
    {
        m_playerTransform = GameObject.Find("Player").transform;
    }

    void Update()
    {
        m_dir.z = m_playerTransform.eulerAngles.y;
        transform.localEulerAngles = m_dir;
    }
}
