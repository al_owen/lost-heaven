using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TellStory : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_audios;
    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private AudioClip m_foundStory, m_notFoundStory;
    [SerializeField] private TrialSTO m_story;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void playAudio(int index)
    {
        if (!m_audioSource.isPlaying)
        {
            m_audioSource.PlayOneShot(m_audios[index]);
        }
    }

    public void PlayStory()
    {
        if (!m_audioSource.isPlaying)
        {
            if (m_story.Found)
            {
                m_audioSource.PlayOneShot(m_foundStory);
            }
            else
            {
                m_audioSource.PlayOneShot(m_notFoundStory);
            }
        }
    }
}
