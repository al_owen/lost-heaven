using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;

public class BatterySlider : MonoBehaviour
{

    [SerializeField] private Slider m_slider;
    [SerializeField] private Gradient m_gradient;
    [SerializeField] private Image m_fill;
    [SerializeField] private FlashlightSTO m_flashlight;
    [SerializeField] private TextMeshProUGUI m_quantity;
    [SerializeField] private Image m_state;
    [SerializeField] private Sprite[] m_states;

    // Start is called before the first frame update
    void Start()
    {
        SetMaxBattery();
        UpdateBatteryLeft();
    }

    
    public void SetMaxBattery()
    {
        m_slider.maxValue = m_flashlight.MaxBattery;
        m_slider.value = m_flashlight.CurrentBattery;

        m_fill.color = m_gradient.Evaluate(1f);
        m_quantity.text = m_flashlight.BatteriesLeft +" X ";
    }

    [Button]
    public void UpdateBatteryLeft()
    {
        m_slider.value = m_flashlight.CurrentBattery;
        m_fill.color = m_gradient.Evaluate(m_slider.normalizedValue);
        m_quantity.text = m_flashlight.BatteriesLeft + " X ";
    }

    public void DisplayState()
    {
        switch (m_flashlight.CurrentMode)
        {
            case LightMode.off:
                m_state.sprite = m_states[0];
                break;
            case LightMode.on:
                m_state.sprite = m_states[1];
                break;
            case LightMode.full:
                m_state.sprite = m_states[2];
                break;
        }
    }
}
