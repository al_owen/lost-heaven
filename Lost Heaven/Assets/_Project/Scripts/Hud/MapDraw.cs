using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;


public class MapDraw : MonoBehaviour
{
    public MapSTO m_map;
    [SerializeField] private Transform m_pins;

    private void Awake()
    {
        
    }

    private void Start()
    {
        int index = 0;
        foreach (RectTransform pin in m_pins)
        {
            pin.position = m_map.PinPositions[index];
            index++;
        }
    }

}
