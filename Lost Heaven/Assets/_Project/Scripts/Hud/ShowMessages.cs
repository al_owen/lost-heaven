using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShowMessages : MonoBehaviour
{
    private Text m_dialog;

    private void Awake()
    {
        m_dialog = transform.Find("Dialog").GetComponent<Text>();
    }

    public void ShowPickUpMessage(bool show)
    {
        m_dialog.text = (show) ? "Press E to pick up": "";
    }

    public void ShowMessage(string message)
    {
        m_dialog.text = message;
    }
}
