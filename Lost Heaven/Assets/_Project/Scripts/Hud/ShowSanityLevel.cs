using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShowSanityLevel : MonoBehaviour
{
    [SerializeField] private Image m_sanityBar;
    [SerializeField] private CharacterSTO m_player;

    // Start is called before the first frame update
    void Start()
    {
        m_sanityBar = this.GetComponent<Image>();
        UpdateSanity();
    }

    public void UpdateSanity()
    {
        m_sanityBar.fillAmount = m_player.CorruptedLevel / m_player.FullCorruption;
    }
}
