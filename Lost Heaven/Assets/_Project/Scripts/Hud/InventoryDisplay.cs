using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InventoryDisplay : MonoBehaviour
{
    [SerializeField] private CharacterSTO m_player;
    [SerializeField] private PuzzleInventorySTO m_inventory;
    //private GameObject m_totem;
    [SerializeField] private Sprite[] m_keysToDisplay;
    private Image m_key;


    // Start is called before the first frame update
    void Start()
    {
        //m_totem = transform.Find("Totem").gameObject;
        m_key = transform.Find("Key").GetComponent<Image>();
        m_key.gameObject.SetActive(false);
        for(int i = 0; i < m_inventory.HasKey.Length; i++)
        {
            if (m_inventory.HasKey[i])
            {
                if (!m_key.gameObject.activeSelf) { m_key.gameObject.SetActive(true); }
                m_key.sprite = m_keysToDisplay[i];
            }
        }
    }

    public void KeyTaken(string keyname)
    {
        if (!m_key.gameObject.activeSelf)
        {
            m_key.gameObject.SetActive(true);
        }
        switch (keyname)
        {
            case "Protection":
                m_key.sprite = m_keysToDisplay[0];
                break;
            case "Shield":
                m_key.sprite = m_keysToDisplay[1];
                break;
            case "Skull":
                m_key.sprite = m_keysToDisplay[2];
                break;
            case "Key":
                m_key.sprite = m_keysToDisplay[3];
                break;
        }
    }
    public void KeyUsed()
    {
        m_key.gameObject.SetActive(false);
    }
}
