using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MarkableMap : MonoBehaviour
{
    [SerializeField] private GameObject m_markPrefab;
    [SerializeField] private bool m_canDraw; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;
            var pin = Instantiate(m_markPrefab, mousePos, Quaternion.identity, this.transform);
            print(mousePos);
        }
        if (Input.GetMouseButtonDown(1))
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

}
