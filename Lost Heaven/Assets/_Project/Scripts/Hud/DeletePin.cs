using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeletePin : MonoBehaviour
{
    private void OnMouseDown()
    {
        Destroy(this.gameObject);
    }
}
