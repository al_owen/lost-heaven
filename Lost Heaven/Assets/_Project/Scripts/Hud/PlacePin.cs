using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class PlacePin : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public MapSTO m_map;
    public int m_pinNumber;

    [SerializeField] private float minWidth = -370f, maxWidth = 370f, minHeight = -200f, maxHeight = 200;
    private bool m_moving = false;

    private void Update()
    {
        if (m_moving)
        {
            RestrictMoveInWindow(this.GetComponent<RectTransform>());
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
        m_moving = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
        m_moving = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        m_map.PinPositions[m_pinNumber] = eventData.position;
        m_moving = false;
    }

    public void RestrictMoveInWindow(RectTransform originTrans)
    {
        Vector3 pos = originTrans.localPosition;

        pos.x = Mathf.Clamp(originTrans.localPosition.x, minWidth, maxWidth);
        pos.y = Mathf.Clamp(originTrans.localPosition.y, minHeight, maxHeight);

        originTrans.localPosition = pos;
    }
}
