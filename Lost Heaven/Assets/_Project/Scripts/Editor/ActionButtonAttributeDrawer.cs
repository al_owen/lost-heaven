using UnityEditor;
using UnityEngine;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector.Editor.ActionResolvers;

public class ActionButtonAttributeDrawer : OdinAttributeDrawer<ActionButtonAttribute>
{
    private ActionResolver actionResolver;

    protected override void Initialize()
    {
        this.actionResolver = ActionResolver.Get(this.Property, this.Attribute.action);
    }

    protected override void DrawPropertyLayout(GUIContent label)
    {
        this.actionResolver.DrawError();

        if (GUILayout.Button("PerformAction"))
        {
            this.actionResolver.DoActionForAllSelectionIndices();
        }

        this.CallNextDrawer(label);
    }
}
