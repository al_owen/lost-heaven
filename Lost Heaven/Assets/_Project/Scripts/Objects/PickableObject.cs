﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableObject : MonoBehaviour, IPickable
{
    [SerializeField] private bool _pickable;
    public bool Pickable {get => _pickable; set => _pickable = value;}
    [SerializeField] private StringGameEvent m_enableHint;

    private void Awake()
    {
        Pickable = true;
    }
   

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "InteractionZone")
        {
            PickUpObject(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
         if (other.tag == "InteractionZone")
         {
            DropObject(other.gameObject);
         }
    }
    
    public void PickUpObject(GameObject collector)
    {
        collector.GetComponentInParent<PickUpObjects>().ObjectToPickUp = this.gameObject;
        m_enableHint.Raise("Press E to pick up the object");
    }

    public void DropObject(GameObject collector)
    {
        collector.GetComponentInParent<PickUpObjects>().ObjectToPickUp = null;
        m_enableHint.Raise("");
    }
}
