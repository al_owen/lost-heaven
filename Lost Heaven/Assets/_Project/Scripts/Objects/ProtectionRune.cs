using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SensorToolkit;

public class ProtectionRune : MonoBehaviour
{
    private bool m_isPetrified = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            //other.GetComponent<Enemy>().Petrify();
            //m_isPetrified = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy" && !m_isPetrified)
        {
            //other.GetComponent<Enemy>().Petrify();
            //m_isPetrified = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            //other.GetComponent<Enemy>().Unpetrify();
            //m_isPetrified = false;
        }
    }

}
