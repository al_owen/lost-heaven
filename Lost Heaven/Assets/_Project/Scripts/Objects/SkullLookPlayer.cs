using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullLookPlayer : MonoBehaviour
{
    [SerializeField]private Transform m_player;

    // Start is called before the first frame update
    void Awake()
    {
        m_player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        var lookPos = m_player.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);
    }
}
