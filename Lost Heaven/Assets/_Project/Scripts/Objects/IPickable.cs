﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickable
{
    /// <summary>
    /// States whether the object can be picked up or not
    /// </summary>
    /// <value></value>
    bool Pickable{get; set;}

    /// <summary>
    /// behavior when the object is picked up 
    /// </summary>
    /// <param name="collector">Collector of the object</param>
    void PickUpObject(GameObject collector);

    /// <summary>
    /// behavior when the object is dropped down 
    /// </summary>
    /// <param name="collector">Collector of the object</param>
    void DropObject(GameObject collector);
}
