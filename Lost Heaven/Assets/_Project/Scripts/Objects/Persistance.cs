using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persistance : MonoBehaviour
{
    [SerializeField] private PersistableSTO m_place;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = m_place.CurrentPosition;
        //this.gameObject.SetActive(m_place.Active);
    }

    private void OnDestroy()
    {
        //PlaceItem(true);
    }

    public void PlaceItem(bool active)
    {
        m_place.CurrentPosition = this.transform.position;
        m_place.Active = active;
    }
}
