using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RitualOrder : MonoBehaviour
{

    [SerializeField] private bool m_isCorrectRune;
    [SerializeField] private bool m_hasRune;
    [SerializeField] private Runes m_rune;
    private GameObject m_runeInPlace = null;
    private Grave m_grave;

    private void Awake()
    {
        m_grave = transform.parent.parent.GetComponent<Grave>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GraveRunes")
        {
            
            if (!m_isCorrectRune)
            {
                if (other.GetComponent<SmallRunesPuzzle>().CorrectRune == m_rune)
                {
                    m_runeInPlace = other.gameObject;
                    m_isCorrectRune = true;
                    m_grave.PlaceRune(1);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "GraveRunes" && m_isCorrectRune && m_runeInPlace == other.gameObject)
        {
            m_runeInPlace = null;
            m_isCorrectRune = false;
            m_grave.PlaceRune(-1);
        }
    }
    
}

public enum Runes
{
    Thurizas, Raido, Wunjo, Jera, Aelghiz, Teiwaz
}