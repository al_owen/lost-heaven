using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRunesPuzzle : MonoBehaviour
{
    [SerializeField] private Runes m_correctRune;
    private Vector3 m_Offset;
    private float m_ZCoord;

    public Runes CorrectRune { get => m_correctRune; set => m_correctRune = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnMouseDown()
    {
        if (!Grave.PuzzleSolved)
        {
            m_ZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
            m_Offset = gameObject.transform.position - GetMouseWorldPos();
        }
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = m_ZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void OnMouseDrag()
    {
        if (!Grave.PuzzleSolved)
        {
            transform.position = GetMouseWorldPos() + m_Offset;
        }
    }

    
}
