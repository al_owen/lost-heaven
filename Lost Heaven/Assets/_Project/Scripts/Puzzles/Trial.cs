using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trial : MonoBehaviour
{
    [SerializeField] private TrialSTO m_trial;
    [SerializeField] private StringGameEvent m_onRead;
    [SerializeField] private GameEventSTO m_close, m_leave;
    private bool m_inRange, m_openDocument;


    private void Awake()
    {
        
    }

    void Start()
    {
        
    }

    private void Update()
    {
        if (m_inRange)
        {
            if (Input.GetKeyDown(KeyCode.E) && !m_openDocument)
            {
                m_openDocument = true;
                m_onRead.Raise(m_trial.Story);
                SoundManager.PlaySFX(m_trial.NarratedStory);
            }
            else if (Input.GetKeyDown(KeyCode.E) && m_openDocument)
            {
                m_openDocument = false;
                m_leave.Raise();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "InteractionZone")
        {
            m_close.Raise();
            m_inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "InteractionZone")
        {
            m_leave.Raise();
            m_inRange = false;
        }
    }

}
