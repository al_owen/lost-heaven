using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Judgement : MonoBehaviour
{
    [SerializeField] private PuzzleSTO m_puzzle;
    [SerializeField] private GameObject m_skull, m_shield, m_grave, m_fakeGraveActive, m_fakeGraveUnactive;
    [SerializeField] private GameObject[] m_papyrus;

    // Start is called before the first frame update
    void Awake()
    {
        m_grave.GetComponent<Animator>().enabled = false;
        m_skull.SetActive(!m_puzzle.RewardTaken);
        m_fakeGraveActive.SetActive(m_puzzle.FinishedPuzzle);
        m_fakeGraveUnactive.SetActive(m_puzzle.ActivePuzzle && !m_puzzle.FinishedPuzzle);
        m_grave.SetActive(!m_puzzle.FinishedPuzzle);
        m_shield.SetActive(m_puzzle.ActiveElements[0]);
        EnablePapyrus();
    }


    public void ActivatePuzzle()
    {
        m_puzzle.ActivePuzzle = true;
        m_grave.GetComponent<Animator>().enabled = true;
        m_puzzle.ActiveElements[0] = true;
    }

    public void TakeSkull(string key)
    {
        if(key == "Skull")
        {
            m_puzzle.ActivePuzzle = false;
            m_puzzle.RewardTaken = true;
        }
    }

    public void EnablePapyrus()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "Forest")
        {
            ActivatePoem(false);
        }
        else if (sceneName == "LostHeaven")
        {
            ActivatePoem(true);
        }
    }

    public void ActivatePoem(bool activatePoem)
    {
        for (int i = 0; i < m_papyrus.Length; i++)
        {
            if (m_papyrus[i].name == "PapyrusPoem")
            {
                m_papyrus[i].SetActive(activatePoem);
            }
            else
            {
                m_papyrus[i].SetActive(!activatePoem);
            }
        }
    }
}
