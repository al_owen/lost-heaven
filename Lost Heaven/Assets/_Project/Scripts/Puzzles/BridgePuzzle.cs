using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BridgePuzzle : Puzzle
{
    [SerializeField] private GameObject m_bridge, m_skull, m_key, m_rune;
    [SerializeField] private PuzzleInventorySTO m_inventory;

    // Start is called before the first frame update
    void Awake()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "Forest") { m_bridge.SetActive(m_puzzleInfo.FinishedPuzzle); }
        else if (sceneName == "LostHeaven") { m_bridge.SetActive(true); }

        m_rune.SetActive(m_puzzleInfo.FinishedPuzzle);
        m_skull.SetActive(m_puzzleInfo.ActivePuzzle);
        bool activate = (!m_puzzleInfo.RewardTaken && m_puzzleInfo.FinishedPuzzle);
        m_key.SetActive(activate);
    }

    public void PlaceSkull()
    {
        m_skull.SetActive(true);
        m_puzzleInfo.ActivePuzzle = true;
        m_puzzleInfo.FinishedPuzzle = true;
        m_inventory.HasKey[2] = false;
        m_inventory.UsedKeys[2] = true;
        m_key.SetActive(true);
        m_rune.SetActive(true);
    }

    public void TakeKey(string key)
    {
        if(key == "Key")
        {
            m_inventory.HasKey[3] = true;
            m_puzzleInfo.RewardTaken = true;
            print("Taken key");
        }
    }
}
