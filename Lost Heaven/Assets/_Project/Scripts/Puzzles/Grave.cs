using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Grave : MonoBehaviour
{
    private static bool m_puzzleSolved = false;
    private int m_correctRunes= 0;
    [SerializeField] private GameObject m_skull;
    [SerializeField] private PuzzleSTO m_trial;
    [SerializeField] private Light[] m_lights;
    [SerializeField] private GameSaver m_saver;

    public static bool PuzzleSolved { get => m_puzzleSolved; private set => m_puzzleSolved = value; }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void PlaceRune(int rune)
    {
        m_correctRunes += rune;
        if (m_correctRunes == 6)
        {
            m_skull.GetComponent<Animation>().Play();
            m_trial.ActivePuzzle = false;
            m_trial.ActiveElements[0] = true;
            StartCoroutine(SolvedPuzzle());
        }
        else if (m_correctRunes <= 0)
        {
            m_correctRunes = 0;
        }
        print(m_correctRunes);
    }

    IEnumerator SolvedPuzzle()
    {
        yield return new WaitForSeconds(1f);
        PuzzleSolved = true;
        m_trial.FinishedPuzzle = true;
        foreach(Light light in m_lights)
        {
            light.color = Color.red;
        }
        yield return new WaitForSeconds(1f);
        m_saver.LoadGame(true);
    }
}
