using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.LSS;

public class GoToHighlighPuzzle : MonoBehaviour
{
    [SerializeField] private LoadingScreenManager m_lsm;
    private bool m_playerInRange = false;

    void Start()
    {
        
    }

    private void Update()
    {
        if (m_playerInRange)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                m_lsm.LoadScene("HighlightPuzzle");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            m_playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            m_playerInRange = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !m_playerInRange)
        {
            m_playerInRange = true;
        }
    }
}
