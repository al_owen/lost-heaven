using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class DisplayDocuments : MonoBehaviour
{
    private GameObject m_document;
    [SerializeField] private Text m_documentText, m_dialog;

    // Start is called before the first frame update
    void Awake()
    {
        m_document = transform.Find("Papyrus").gameObject;
        m_documentText = m_document.transform.GetChild(0).GetComponent<Text>();
        m_dialog = transform.Find("Dialog").gameObject.GetComponent<Text>();
        CloseDocument();
    }

    public void ShowDocument(string trialDoc)
    {
        //m_dialog.text = trialDoc;
        m_document.SetActive(true);
        m_documentText.text = trialDoc;
    }

    public void CloseDocument()
    {
        m_dialog.text = "";
        m_document.SetActive(false);
    }

    public void NextToDocument()
    {
        m_dialog.text = "Press E to read";
    }
}
