using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergySource : MonoBehaviour
{
    [SerializeField] private bool m_hasEnergy = false;
    [SerializeField] private ColorsPuzzle puzzle;

    private void Awake()
    {
        m_hasEnergy = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Protection")
        {
            Activate();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Protection" && !m_hasEnergy)
        {
            Activate();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Protection")
        {
            Deactivate();
        }
    }

    public void Deactivate()
    {
        m_hasEnergy = false;
        puzzle.Activate(false);
    }

    public void Activate()
    {
        if(!ColorsPuzzle.m_areColorsSet)
        {
            puzzle.RestartLights();
        }
        m_hasEnergy = true;
        puzzle.Activate(true);
    }
}
