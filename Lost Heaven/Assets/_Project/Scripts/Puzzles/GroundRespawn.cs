using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GroundRespawn : MonoBehaviour
{
    [SerializeField] private int m_sanityIncrease = 5;
    [SerializeField] private Vector3 m_respawnPos = new Vector3(538f, 24f, 656f);

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<Corruption>().Corrupt(m_sanityIncrease);
            other.GetComponent<FirstPersonController>().Teleport(m_respawnPos);
        }
    }
}
