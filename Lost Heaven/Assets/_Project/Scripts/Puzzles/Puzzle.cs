using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    [SerializeField] protected PuzzleSTO m_puzzleInfo;
    [SerializeField] protected GameObject[] m_objectsToEnable;

    public void Init()
    {
        int elements = m_puzzleInfo.ActiveElements.Length;
        if (m_objectsToEnable.Length == elements)
        {
            int index = 0;
            foreach(GameObject objects in m_objectsToEnable)
            {
                objects.SetActive(m_puzzleInfo.ActiveElements[index]);
                index++;
            }
        }
        else
        {
            print("some error");
        }
    }

}
