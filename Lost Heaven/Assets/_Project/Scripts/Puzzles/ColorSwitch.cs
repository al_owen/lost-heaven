using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwitch : MonoBehaviour
{
    private ColorsPuzzle m_colorsPuzzle;
    private Color m_lightColor;

    // Start is called before the first frame update
    void Start()
    {
        m_colorsPuzzle = this.transform.parent.gameObject.GetComponent<ColorsPuzzle>();
        m_lightColor = this.transform.Find("torch").Find("Fire").gameObject.GetComponent<Light>().color;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "PlaceableBox")
        {
            m_colorsPuzzle.ColorSelected(m_lightColor);
        }
    }
}
