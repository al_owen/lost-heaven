using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ColorsPuzzle : Puzzle
{
    public static bool m_areColorsSet = false;
    [SerializeField] private static int m_correctColors;
    [SerializeField] private GameObject m_mainTorch, m_totemKey, m_placedTotem, m_box, m_shield;
    [SerializeField] private static List<Color> Colors;
    [SerializeField] private List<Color> m_lights;
    private Color m_currentColor;
    [SerializeField] private PuzzleInventorySTO m_inventory;

    private void Awake()
    {
        m_mainTorch = this.transform.Find("MainTorch").transform.Find("Fire").gameObject;
        if (!m_areColorsSet)
        {
            RestartLights();
        }
        PlaceColors();
        Activate(m_inventory.UsedKeys[0]);
        Init();
        m_mainTorch.GetComponent<Light>().color = m_currentColor;
        bool active = (!m_inventory.HasKey[0] && !m_inventory.UsedKeys[0]) ? true : false;
        m_totemKey.SetActive(active);
        m_placedTotem.SetActive(m_inventory.UsedKeys[0]);
        m_box.SetActive(!m_puzzleInfo.FinishedPuzzle);
        bool activeShield = (!m_puzzleInfo.RewardTaken && m_puzzleInfo.FinishedPuzzle);
        m_shield.SetActive(activeShield);
    }

    public void RestartLights()
    {
        RandomizeColors();
        m_currentColor = Colors[0];
        m_mainTorch.GetComponent<Light>().color = m_currentColor;
        m_areColorsSet = true;
        PlaceColors();
    }

    public void PlaceColors()
    {
        int index = 0;
        
        foreach (GameObject light in m_objectsToEnable)
        {
            light.GetComponent<Light>().color = Colors[index];
            index++;
        }
        m_currentColor = Colors[m_correctColors];
    }
    
    public void Activate(bool active)
    {
        if (active)
        {
            
            if (SceneManager.GetActiveScene().name == "Forest")
            {
                m_puzzleInfo.ActiveElements[1] = true;
                m_puzzleInfo.ActiveElements[3] = true;
                m_puzzleInfo.ActiveElements[5] = true;

                m_puzzleInfo.ActiveElements[0] = false;
                m_puzzleInfo.ActiveElements[2] = false;
                m_puzzleInfo.ActiveElements[4] = false;
            }
            else if (SceneManager.GetActiveScene().name == "LostHeaven")
            {
                m_puzzleInfo.ActiveElements[0] = true;
                m_puzzleInfo.ActiveElements[2] = true;
                m_puzzleInfo.ActiveElements[4] = true;

                m_puzzleInfo.ActiveElements[1] = false;
                m_puzzleInfo.ActiveElements[3] = false;
                m_puzzleInfo.ActiveElements[5] = false;
            }
        }
        else
        {
            m_puzzleInfo.ActiveElements[0] = false;
            m_puzzleInfo.ActiveElements[1] = false;
            m_puzzleInfo.ActiveElements[2] = false;
            m_puzzleInfo.ActiveElements[3] = false;
            m_puzzleInfo.ActiveElements[4] = false;
            m_puzzleInfo.ActiveElements[5] = false;
        }
        m_puzzleInfo.ActivePuzzle = active;
        Init();
    }

    public void ColorSelected(Color color)
    {
        if(m_correctColors < m_lights.Count - 1)
        {
            if (color == m_currentColor)
            {
                m_correctColors++;
                m_currentColor = m_lights[m_correctColors];
                m_mainTorch.GetComponent<Light>().color = m_currentColor;
            }
            else
            {
                m_correctColors = 0;
                RestartLights();
            }
            //print("correct "+m_correctColors+" current color "+m_currentColor+" total colors "+m_lights.Count);
        }
        else if (m_correctColors == m_lights.Count - 1)
        {
            m_puzzleInfo.FinishedPuzzle = true;
            m_shield.transform.position = m_box.transform.position;
            m_shield.SetActive(true);
            m_box.SetActive(false);
        }
    }

    public void RandomizeColors()
    {
        for (int i = 0; i < m_lights.Count; i++)
        {
            Color temp = m_lights[i];
            int randomIndex = Random.Range(i, m_lights.Count);
            m_lights[i] = m_lights[randomIndex];
            m_lights[randomIndex] = temp;
        }
        Colors = m_lights;
    }

    public void TakeShield(string key)
    {
        if(key == "Shield")
        {
            m_puzzleInfo.RewardTaken = true;
        }
    }
}
