using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;
using Managers;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
    private NavMeshAgent m_agent;
    private Transform m_player;
    private EnemyStates m_currentState;
    private Animator m_animator;
    private GameObject m_ragdoll, m_body;
    private CapsuleCollider m_collider;

    [Header("Return to initial point")]
    [SerializeField] private Vector3 m_initialPoint;
    [SerializeField] private bool m_walkPointSet;
    //[SerializeField] private float m_walkPointRange;

    [Header("Attacking")]
    [SerializeField] private float m_TimeBetweenAttacks = 1f;
    private bool m_alreadyAttack = false;

    [Header("States")]
    [SerializeField] private float m_sightRange;
    [SerializeField] private bool m_playerInRange, m_decoyInRange;

    public Transform Player { get => m_player; set => m_player = value; }

    public enum EnemyStates
    {
        Chase, Patrol, Attack, Idle, GiveUp, Petrify, Dead
    }

    // Start is called before the first frame update
    void Awake()
    {
        m_collider = transform.GetComponent<CapsuleCollider>();
        m_ragdoll = this.transform.Find("Ragdoll").gameObject;
        m_body = this.transform.Find("Body").gameObject;
        m_initialPoint = transform.position;
        m_playerInRange = false;
        m_alreadyAttack = false;
        m_animator = this.transform.GetChild(0).GetComponent<Animator>();
        m_agent = GetComponent<NavMeshAgent>();
        Player = GameObject.Find("Player").transform;
        m_currentState = EnemyStates.Idle;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_currentState == EnemyStates.Petrify || m_currentState == EnemyStates.Dead || !GameManager.Instance.HasBeenLoaded)
        {
            return;
        }

        if (m_playerInRange)
        {
            if(m_currentState == EnemyStates.Chase)
            {
                if (GameManager.Instance.PlayerInSafeHouse) { StopChasing(); return; }

                m_agent.destination = Player.position;
                Vector3 distanceToPlayer = transform.position - Player.position;
                FacePlayer();

                if (distanceToPlayer.magnitude <= m_agent.stoppingDistance)
                {
                    AttackPlayer();
                }
            }
        }
        else
        {
            if(m_currentState == EnemyStates.GiveUp)
            {
                ReturningToBase();
            }
        }
        Physics.SyncTransforms();
    }

    public void Petrify(float time)
    {
        if(m_currentState != EnemyStates.Petrify)
        {
            print("Ive been hit");
            m_currentState = EnemyStates.Petrify;
            m_animator.SetBool("Chasing", false);
            m_animator.SetTrigger("Petrify");
            m_agent.destination = this.transform.position;
            StartCoroutine(PetrificationTime(time));
        }
    }

    public void Unpetrify()
    {
        print("I am alive");
        m_currentState = EnemyStates.Idle;
        if (m_playerInRange)
        {
            Chase();
        }
        else
        {
            ReturningToBase();
        }
    }

    public void Die()
    {
        m_currentState = EnemyStates.Dead;
        m_body.SetActive(false);
        m_ragdoll.SetActive(true);
        m_ragdoll.transform.parent = null;
        this.gameObject.SetActive(false);
    }

    private void FacePlayer()
    {
        Vector3 direction = (Player.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 4f);
    }

    public void Chase()
    {
        if (!GameManager.Instance.PlayerInSafeHouse && m_currentState != EnemyStates.Petrify)
        {
            Player = GameObject.Find("Player").transform;
            m_playerInRange = true;
            m_walkPointSet = false;
            m_animator.SetBool("Chasing", true);
            StartCoroutine(StartChase());
        }
       
    }

    public void AttackPlayer()
    {
        if (!m_alreadyAttack && m_currentState != EnemyStates.Petrify)
        {
            m_agent.SetDestination(transform.position);
            //transform.LookAt(m_player);
            m_animator.SetTrigger("Attack");
            m_alreadyAttack = true;
            StartCoroutine(MakeAttack());
        }
    }

    public void StopChasing()
    {
        if (m_currentState != EnemyStates.Petrify)
        {
            m_playerInRange = false;
            m_currentState = EnemyStates.Idle;
            m_animator.SetBool("Chasing", false);
            StartCoroutine(GiveUp());
        }
    }

    public void ReturningToBase()
    {
        Vector3 distanceToPoint = transform.position - m_initialPoint;

        if (distanceToPoint.magnitude < 2f)
        {
            m_walkPointSet = false;
            m_animator.SetBool("Chasing", false);
        }
    }

    public void SearchWalkPoint()
    {
        if (!m_walkPointSet)
        {
            m_agent.destination = m_initialPoint;
            m_animator.SetBool("Chasing", true);
            m_walkPointSet = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Corruption>().Corrupt(5);
        }
        if (other.tag == "Fire")
        {
            Die();
        }
        
    }

    IEnumerator GiveUp()
    {
        yield return new WaitForSeconds(6f);
        if (!m_playerInRange && m_currentState != EnemyStates.Chase && m_currentState != EnemyStates.Petrify)
        {
            m_currentState = EnemyStates.GiveUp;
            SearchWalkPoint();
        }
        //yield return new WaitForSeconds(5f);
        
            
    }

    IEnumerator StartChase()
    {
        yield return new WaitForSeconds(0.05f);
        m_currentState = EnemyStates.Chase;
    }

    IEnumerator PetrificationTime(float time)
    {
        yield return new WaitForSeconds(time);
        Unpetrify();
    }

    IEnumerator MakeAttack()
    {
        yield return new WaitForSeconds(m_TimeBetweenAttacks);
        m_alreadyAttack = false;
    }
}
