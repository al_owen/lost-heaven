using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestLimit : MonoBehaviour
{
    [SerializeField] private Material m_dissolve;
    [SerializeField] private float m_speed = 1f;
    private SoundLimit m_soundLimit;

    private void Awake()
    {
        m_dissolve.SetFloat("Vector1_3b51b90b409f42c1ad4841d11bfd1f19", m_speed);
        m_soundLimit = transform.parent.GetComponent<SoundLimit>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StopAllCoroutines();
            StartCoroutine(Dissolve(1f, 0f, 1f));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            StopAllCoroutines();
            StartCoroutine(MakeTransparent(0f, 1f, 1f));
            m_soundLimit.MakeSound();
        }
    }

    IEnumerator Dissolve(float v_start, float v_end, float duration)
    {
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            m_speed = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            m_dissolve.SetFloat("Vector1_3b51b90b409f42c1ad4841d11bfd1f19", elapsed);
            yield return null;
        }
        m_speed = v_end;
    }

    IEnumerator MakeTransparent(float v_start, float v_end, float duration)
    {
        float elapsed = duration;
        while (elapsed > 0)
        {
            m_speed = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed -= Time.deltaTime;
            m_dissolve.SetFloat("Vector1_3b51b90b409f42c1ad4841d11bfd1f19", elapsed);
            yield return null;
        }
        m_speed = v_end;
    }

    private void OnDestroy()
    {
        m_dissolve.SetFloat("Vector1_3b51b90b409f42c1ad4841d11bfd1f19", 1);
    }
}
