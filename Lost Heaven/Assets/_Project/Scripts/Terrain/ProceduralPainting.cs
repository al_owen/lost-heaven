﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ProceduralPainting : MonoBehaviour
{
    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startingHeight;
        public int overlap;
    }

    public SplatHeights[] splatHeights;
    public TerrainData tData;
    [Range(0.0f, 0.5f)] [SerializeField] private float _smoothness;

    float GetPerlin(int x, int y) => map(Mathf.PerlinNoise(x * _smoothness, y * _smoothness), 0, 1, 0.5f, 1f);


    // Start is called before the first frame update
    void Start()
    {
        //paintMap();    
    }

    void normalize(float[] colorMap)
    {
        float total = 0;
        for (int i = 0; i< colorMap.Length; i++)
        {
            total += colorMap[i];
        }
        
        for (int i = 0; i < colorMap.Length; i++)
        {
            colorMap[i] /= total;
        }
    }
    [Button]
    public void paintMap()
    {
        float[,,] splatmapData = new float[tData.alphamapWidth, tData.alphamapHeight, tData.alphamapLayers];

        for (int x = 0; x < tData.alphamapWidth; x++)
        {
            for (int y = 0; y < tData.alphamapHeight; y++)
            {
                float terrainHeight = tData.GetHeight(x, y);

                float[] splat = new float[splatHeights.Length];

                for (int i = 0; i < splatHeights.Length; i++)
                {
                    float thisHeightStart = splatHeights[i].startingHeight * GetPerlin(x,y) - splatHeights[i].overlap * GetPerlin(x, y);
                    float nextHeightStart = 0;

                    if (i != splatHeights.Length-1)
                    {
                        nextHeightStart = splatHeights[i + 1].startingHeight * GetPerlin(x, y) + splatHeights[i + 1].overlap * GetPerlin(x, y);
                    }

                    if (i == splatHeights.Length - 1 && terrainHeight >= thisHeightStart)
                    {
                        splat[i] = 1;
                    }
                    else if(terrainHeight >= thisHeightStart && terrainHeight <= nextHeightStart)
                    {
                        splat[i] = 1;
                    }
                }
                normalize(splat);
                for (int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }

            }
        }

        tData.SetAlphamaps(0, 0, splatmapData);
    }

    /// <summary>
    /// Re-maps the values 
    /// </summary>
    /// <param name="value"></param> //takes a value
    /// <param name="sMin"></param> //original minimum range
    /// <param name="sMax"></param> //original maximum range
    /// <param name="mMin"></param> //new minimum range
    /// <param name="mMax"></param> //new maximum range
    /// <returns></returns>
    public float map(float value, float sMin, float sMax, float mMin, float mMax)
    {
        return (value - sMin) * (mMax - mMin) / (sMax - sMin) + mMin;
    }
}
