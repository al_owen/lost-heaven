using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLimit : MonoBehaviour
{
    public SFX sfx;

    public void MakeSound()
    {
        sfx.PlaySFX();
    }
}
