using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using Michsky.LSS;

public class RuneTeleport : MonoBehaviour
{
    public CharacterSTO PlayerPos;
    public RuneSTO Rune;
    private bool m_canInteract = false;
    private string m_currentScene; 
    private string m_TeleportScene;
    [SerializeField] private Transform m_player;
    [SerializeField] private StringGameEvent m_onDisplayMessage;
    [SerializeField] private FlashlightSTO m_flashlight;
    [SerializeField] private AbilitySTO m_ability;
    [SerializeField] private LoadingScreenManager m_lsm;

    private void Awake()
    {
        m_onDisplayMessage = Resources.Load<StringGameEvent>("STOs/Events/string/onMessageDisplay");    
    }

    void Start()
    {
        m_player = null;
        m_currentScene = SceneManager.GetActiveScene().name;
        if (m_currentScene == "Forest")
        {
            m_TeleportScene = "LostHeaven";
        }
        else if(m_currentScene == "LostHeaven")
        {
            m_TeleportScene = "Forest";
        }
    }

    private void Update()
    {
        if (m_canInteract)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                if (m_player != null) 
                { 
                    PlayerPos.CurrentPosition = m_player.position;
                    PlayerPos.ActivePosition = true;
                    m_flashlight.CurrentAbility = m_ability;
                } 
                m_lsm.LoadScene(m_TeleportScene);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            m_player = other.transform;
            m_canInteract = true;
            m_onDisplayMessage.Raise(Rune.Dialog.DialogText[0]+"\n \n Press T to change world");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            m_player = other.transform;
            m_canInteract = true;
            m_onDisplayMessage.Raise(Rune.Dialog.DialogText[0] + "\n \n Press T to change world");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            m_player = null;
            m_canInteract = false;
            m_onDisplayMessage.Raise("");
        }
    }
}
