using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.LSS;

public class CreditsRune : MonoBehaviour
{
    [SerializeField] private LoadingScreenManager m_lsm;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            m_lsm.LoadScene("Credits");
        }
        
    }
}
