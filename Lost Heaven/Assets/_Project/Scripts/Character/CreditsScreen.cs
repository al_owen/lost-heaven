using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Michsky.LSS;

public class CreditsScreen : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera m_virtualCamera;
    [SerializeField] private GameObject m_human, m_draugr;
    [SerializeField] private CharacterSTO m_player;
    [SerializeField] private LoadingScreenManager m_lsm;
    private Transform target;

    private void Awake()
    {

    }

    private void Start()
    {

        if (m_player.CorruptionLevel == CorruptionLevel.FullyCorrupted)
        {
            target = m_draugr.transform;
        }
        else
        {
            target = m_human.transform;
        }

        m_virtualCamera.m_LookAt = target.transform;
        m_virtualCamera.m_Follow = target.transform;
        StartCoroutine(CreditsRoll());
    }

    IEnumerator CreditsRoll()
    {
        yield return new WaitForSeconds(3.5f);
        target.gameObject.SetActive(true);
        yield return new WaitForSeconds(15f);
        m_lsm.LoadScene("Menu");
    }
}
