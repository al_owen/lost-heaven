using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using Michsky.LSS;

public class Interact : MonoBehaviour
{
    [SerializeField] private GameEventSTO m_onInteraction, m_onCheckPoint;
    [SerializeField] private PuzzleInventorySTO m_inventory;
    [SerializeField] private bool m_inZone, m_interacting, m_finishedInteracting = false;
    [SerializeField] private float m_startTime, m_finishTime, m_currentTime = 0f;
    [SerializeField] private int m_keyIndex = 0;
    [SerializeField] private string m_destination = "";
    [SerializeField] private Image m_image = null;
    [SerializeField] private StringGameEvent m_onInteractionRange;
    [SerializeField] private LoadingScreenManager m_lsm;


    // Start is called before the first frame update
    void Start()
    {
        m_image = GameObject.Find("CanvasLH").transform.Find("ProgressBar").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_inZone && !m_finishedInteracting && m_inventory.HasKey[m_keyIndex])
        {
            if (Input.GetKeyDown(KeyCode.Q) && !m_interacting)
            {
                m_interacting = true;
                StartCoroutine(Interaction());
            }
            else if (Input.GetKeyUp(KeyCode.Q) && m_interacting)
            {
                m_interacting = false;
                m_currentTime = m_startTime;
                m_image.fillAmount = 0f;
                StopCoroutine(Interaction());
            }
        }
    }

    public void GoToPuzzleScene()
    {
        StartCoroutine(GoToPuzzle());
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "InteractionZone")
        {
            m_inZone = true;
            m_onInteractionRange.Raise("Hold Q to interact");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "InteractionZone")
        {
            m_currentTime = m_startTime;
            m_image.fillAmount = 0f;
            m_interacting = false;
            m_inZone = false;
            m_onInteractionRange.Raise("");
        }
    }

    private IEnumerator GoToPuzzle()
    {
        yield return new WaitForSeconds(2f);
        m_lsm.LoadScene(m_destination);
    }

    IEnumerator Interaction()
    {
        float timeToWait = 0.016f;
        while (m_interacting)
        {
            m_currentTime += timeToWait;
            m_image.fillAmount = m_currentTime / m_finishTime;
            if (m_currentTime >= m_finishTime)
            {
                m_inventory.HasKey[m_keyIndex] = false;
                m_inventory.UsedKeys[m_keyIndex] = true;
                m_finishedInteracting = true;
                m_image.fillAmount = 0f;
                m_onInteraction.Raise();
                m_interacting = false;
            }
            yield return new WaitForSeconds(timeToWait);
        }
    }
}
