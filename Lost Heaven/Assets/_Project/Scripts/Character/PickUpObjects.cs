﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component that allows the character to pick up objects
/// </summary>
public class PickUpObjects : MonoBehaviour
{
    //////////////// Basic variables /////////
    [SerializeField] private GameObject _objectToPickUp; //object inside the interaction zone
    [SerializeField] private StringGameEvent m_dropMessage;
    public GameObject ObjectToPickUp{get=> _objectToPickUp; set => _objectToPickUp = value;}
    private GameObject _pickedObjects; //picked object
    private Transform _interactionZone; //place where the object will move when picked up
    private bool m_renewedMessage = false;


    // Start is called before the first frame update
    void Awake()
    {
        _interactionZone = this.transform.Find("InteractionZone").gameObject.transform;
        _pickedObjects = null;
        _objectToPickUp = null;
    }

    // Update is called once per frame
    void Update()
    {
        PickUp();
    }

    /// <summary>
    /// method that makes an object children of the character
    /// </summary>
    private void PickUp()
    {

        //if there's an object to pickup and can be picked up and no object is already picked up
        if (_objectToPickUp != null && _objectToPickUp.GetComponent<IPickable>().Pickable && _pickedObjects == null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                _pickedObjects = _objectToPickUp; //pick object
                _pickedObjects.GetComponent<IPickable>().Pickable = false;  //make it unpickable
                _pickedObjects.transform.SetParent(_interactionZone); 
                _pickedObjects.transform.position = _interactionZone.position; //moves to the picked position
                _pickedObjects.GetComponent<Rigidbody>().useGravity = false;    
                _pickedObjects.GetComponent<Rigidbody>().isKinematic = true;
                _pickedObjects.transform.rotation = Quaternion.identity;
            }

        }
        else if(_pickedObjects != null) //if an object is already picked up 
        {
            if (!m_renewedMessage)
            {
                m_dropMessage.Raise("Press E to drop");
                m_renewedMessage = true;
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                _pickedObjects.gameObject.GetComponent<IPickable>().Pickable = true;
                _pickedObjects.transform.SetParent(null);
                _pickedObjects.GetComponent<Rigidbody>().useGravity = true;
                _pickedObjects.GetComponent<Rigidbody>().isKinematic = false;
                _pickedObjects = null;
                m_renewedMessage = false;
            }
        }
        
    }
}
