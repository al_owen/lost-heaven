using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private GameObject m_key;
    [SerializeField] private CharacterSTO m_player;
    [SerializeField] private PuzzleInventorySTO m_inventory;
    private Transform _interactionZone; //place where the object will move when picked up
    [SerializeField] private bool m_keyInRange;

    [Header("Events")]
    [SerializeField] private GameEventSTO m_onPlaceTotem, m_onTakeToken;
    [SerializeField] private StringGameEvent m_onKeyPicked;



    void Start()
    {
        _interactionZone = this.transform.Find("InteractionZone").gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        SaveInInventory();

    }

    public void SaveInInventory()
    { 
        if (m_keyInRange)
        {
            if (Input.GetKeyDown(KeyCode.Q) && m_key != null)
            {
                print(m_key.name);
                m_onKeyPicked.Raise(m_key.name);
                m_key.SetActive(false);
                m_keyInRange = false;
                SaveKey(m_key.name);
            }
        }
    }

    public void SaveKey(string keyname)
    {
        switch (keyname)
        {
            case "Protection":
                m_inventory.HasKey[0] = true;
                break;
            case "Shield":
                m_inventory.HasKey[1] = true;
                break;
            case "Skull":
                m_inventory.HasKey[2] = true;
                break;
            case "Key":
                m_inventory.HasKey[3] = true;
                break;
        }
    }

    public void UseKey(string keyname)
    {
        switch (keyname)
        {
            case "Protection":
                m_inventory.HasKey[0] = false;
                m_inventory.UsedKeys[0] = true;
                break;
            case "Shield":
                m_inventory.HasKey[1] = false;
                m_inventory.UsedKeys[1] = true;
                break;
            case "Skull":
                m_inventory.HasKey[2] = false;
                m_inventory.UsedKeys[2] = true;
                break;
            case "Key":
                m_inventory.HasKey[3] = false;
                m_inventory.UsedKeys[3] = true;
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Key")
        {
            m_keyInRange = true;
            m_key = other.gameObject;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Key" && !m_keyInRange)
        {
            m_keyInRange = true;
            m_key = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Key")
        {
            m_keyInRange = false;
            m_key = null;
        }
    }
}
