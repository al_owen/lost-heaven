using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour, IMultipleGameObjectPooled
{
    private MultipleGameObjectsPool m_pool;
    public MultipleGameObjectsPool Pool { get => m_pool; set => m_pool = value; }

    private void OnEnable()
    {
        StartCoroutine(VisionLife());
    }

    IEnumerator VisionLife()
    {
        yield return new WaitForSeconds(3f);
        m_pool.returnToPool(this.gameObject);
    } 
}
