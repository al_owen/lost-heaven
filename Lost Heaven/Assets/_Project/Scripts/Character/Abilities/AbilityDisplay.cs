using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class AbilityDisplay : MonoBehaviour
{
    public FlashlightSTO m_flashlight;
    
    void Start()
    {
        if(SceneManager.GetActiveScene().name == "LostHeaven")
        {
            this.GetComponent<Image>().sprite = m_flashlight.CurrentAbility.Sprite;
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }

    
}
