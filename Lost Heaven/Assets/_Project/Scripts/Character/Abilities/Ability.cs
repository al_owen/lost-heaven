using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour, IGameObjectPooled
{
    [SerializeField] private AbilitySTO m_ability;

    private GameObjectsPool m_pool;

    public GameObjectsPool Pool { get => m_pool; set => m_pool = value; }

    private void OnEnable()
    {
        StartCoroutine(Die());
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Enemy" && m_ability.CurrentAbilities == Abilities.Decoy)
        {
            other.GetComponent<Enemy>().Player = this.transform;
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(m_ability.Time);
        m_pool.returnToPool(this.gameObject);
    }
}
