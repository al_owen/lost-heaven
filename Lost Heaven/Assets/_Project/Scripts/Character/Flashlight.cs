using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Managers;
using SensorToolkit;
using UnityStandardAssets.Characters.FirstPerson;

public class Flashlight : MonoBehaviour
{
    [SerializeField] private Light m_light;
    [SerializeField] private FlashlightSTO m_flashlight;
    [SerializeField] private GameEventSTO m_OnDrain, m_onChangeMode;
    [SerializeField] private GameObjectsPool m_firePool, m_freezePool, m_decoyPool;
    [SerializeField] private bool m_canShoot;
    private Camera m_camera;
    //private int layerMask = (1 << 6) | (1 << 7);

    private bool InDarkWorld => (SceneManager.GetActiveScene().name == "LostHeaven");

    private void Awake()
    {
        m_camera = Camera.main;
        m_light = transform.GetChild(0).GetChild(0).GetComponent<Light>();
        TurnOffFlashlight();
        m_canShoot = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !GameManager.Instance.Paused && GameManager.Instance.HasBeenLoaded)
        {
            ChangeLightMode();
            SoundManager.PlaySFX(SoundManager.Instance.BasicSounds[0]);
        }
        else if (Input.GetKeyDown(KeyCode.R) && !GameManager.Instance.Paused && GameManager.Instance.HasBeenLoaded)
        {
            SoundManager.PlaySFX(SoundManager.Instance.BasicSounds[0]);
            m_flashlight.Recharge();
            m_OnDrain.Raise();
        }
        if (Input.GetMouseButtonDown(1) && m_flashlight.CurrentMode != LightMode.off && InDarkWorld && m_canShoot)
        {
            ShootAbility();
        }
    }

    private void ShootAbility()
    {
        if (m_flashlight.BatteriesLeft < m_flashlight.CurrentAbility.Usage) { return; }

        float range = m_flashlight.CurrentAbility.Range;
        var layerMask = m_flashlight.CurrentAbility.Layers;
        RaycastHit hit;
        if (Physics.Raycast(m_camera.transform.position, m_camera.transform.forward, out hit, range, layerMask))
        {
            switch (m_flashlight.CurrentAbility.CurrentAbilities)
            {
                case Abilities.Paralize:
                    if (hit.collider.tag == "Enemy")
                    {
                        print(hit.collider.name);
                        SpawnAbility(m_freezePool.Get(), hit);
                        hit.collider.GetComponent<Enemy>().Petrify(m_flashlight.CurrentAbility.Time);
                    }
                    break;
                case Abilities.Fire:
                    SpawnAbility(m_firePool.Get(), hit);
                    break;
                case Abilities.Teleport:
                    this.GetComponent<FirstPersonController>().Teleport(hit.point);
                    UseAbility();
                    break;
                case Abilities.Decoy:
                    SpawnAbility(m_decoyPool.Get(), hit);
                    break;
            }
        }
    }

    private void SpawnAbility(GameObject ability, RaycastHit hit)
    {
        ability.transform.position = hit.point;
        ability.SetActive(true);
        UseAbility();
    }

    private void UseAbility()
    {
        m_flashlight.BatteriesLeft -= m_flashlight.CurrentAbility.Usage;
        m_OnDrain.Raise();
        //StartCoroutine(Cooldown());
    }

    public void ChangeLightMode()
    {
        switch (m_flashlight.CurrentMode)
        {
            case LightMode.off:
                TurnOnFlashlight();
                break;
            case LightMode.on:
                TurnFullFlashlight();
                break;
            case LightMode.full:
                TurnOffFlashlight();
                break;
        }
        m_onChangeMode.Raise();
    }

    public void TurnOffFlashlight()
    {
        m_flashlight.CurrentMode = LightMode.off;
        m_light.gameObject.SetActive(false);
        StopAllCoroutines();
        m_onChangeMode.Raise();
    }

    public void TurnOnFlashlight()
    {
        if (m_flashlight.IsEmpty || m_flashlight.CurrentBattery <= 0) { return; }

        m_flashlight.CurrentMode = LightMode.on;
        m_light.gameObject.SetActive(true);
        m_light.intensity = m_flashlight.HalfIntensity;
        StartCoroutine(UseBattery());
    }

    public void TurnFullFlashlight()
    {
        if (m_flashlight.IsEmpty || m_flashlight.CurrentBattery <= 0) { return; }

        if (!m_light.gameObject.activeSelf)
        {
            m_light.gameObject.SetActive(true);
        }
        m_flashlight.CurrentMode = LightMode.full;
        m_light.intensity = m_flashlight.FullIntensity;
    }


    IEnumerator UseBattery()
    {
        while (m_flashlight.CurrentMode != LightMode.off && !m_flashlight.IsEmpty)
        {
            m_flashlight.Drain();
            if(m_flashlight.CurrentBattery <= 0) { TurnOffFlashlight(); }
            m_OnDrain.Raise();
            yield return new WaitForSeconds(4f);
        }
    }

    IEnumerator Cooldown()
    {
        m_canShoot = false;
        print(m_flashlight.CurrentAbility.Time);
        yield return new WaitForSeconds(m_flashlight.CurrentAbility.Time);
        m_canShoot = true;
    }
}
public enum LightMode
{
    off, on, full
}

public enum Abilities
{
    Fire, Paralize, Decoy, Teleport
}
