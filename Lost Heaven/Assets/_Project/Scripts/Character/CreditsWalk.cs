using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsWalk : MonoBehaviour
{
    
    [SerializeField] private Vector3 target = new Vector3(553f, 10.2f, 615.25f);
    [SerializeField] private float speed = 1f;

    private void Update()
    {
        // Moves the object to target position
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
    }

}
