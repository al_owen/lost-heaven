using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.LSS;

public class Corruption : MonoBehaviour
{
    [SerializeField] private CharacterSTO m_player;
    [SerializeField] private GameEventSTO m_onHurt;
    [SerializeField] private LoadingScreenManager m_lsm;
    [SerializeField] private float m_visionFrequency = 30f;
    [SerializeField] private MultipleGameObjectsPool m_visionsPool;
    [SerializeField] private Light m_LHSettings;
    [SerializeField] private Color m_dark, m_light;

    private bool m_affected => m_player.CorruptionLevel == CorruptionLevel.Level3 || m_player.CorruptionLevel == CorruptionLevel.Level4 || m_player.CorruptionLevel == CorruptionLevel.Level5;
        
    // Start is called before the first frame update
    void Start()
    {
        CheckFrequency();
        StartCoroutine(Visions());
    }

    public void Corrupt(int corruptionLevel = 1) 
    {
        m_player.CorruptedLevel += corruptionLevel;
        switch (m_player.CorruptionLevel)
        {
            case CorruptionLevel.NotCorrupted:
                if(m_player.CorruptedLevel >= 10f)
                {
                    m_player.CorruptionLevel = CorruptionLevel.Level1;
                    m_player.WalkingSpeed = 4f;
                    m_player.RunningSpeed = 9f;
                }
                break;
            case CorruptionLevel.Level1:
                if (m_player.CorruptedLevel >= 25f)
                {
                    m_player.CorruptionLevel = CorruptionLevel.Level2;
                    m_player.WalkingSpeed = 3.5f;
                    m_player.RunningSpeed = 8.5f;
                }
                break;
            case CorruptionLevel.Level2:
                if (m_player.CorruptedLevel >= 40f)
                {
                    m_player.CorruptionLevel = CorruptionLevel.Level3;
                    CheckFrequency();
                    StopAllCoroutines();
                    StartCoroutine(Visions());
                }
                break;
            case CorruptionLevel.Level3:
                if (m_player.CorruptedLevel >= 55f)
                {
                    m_player.CorruptionLevel = CorruptionLevel.Level4;
                    CheckFrequency();
                    StopAllCoroutines();
                    StartCoroutine(Visions());
                }
                break;
            case CorruptionLevel.Level4:
                if (m_player.CorruptedLevel >= 75f)
                {
                    m_player.CorruptionLevel = CorruptionLevel.Level5;
                    CheckFrequency();
                    StopAllCoroutines();
                    StartCoroutine(Visions());
                }
                break;
            case CorruptionLevel.Level5:
                if (m_player.CorruptedLevel >= 100f)
                {
                    m_player.CorruptedLevel = 100;
                    m_player.CorruptionLevel = CorruptionLevel.FullyCorrupted;
                    m_lsm.LoadScene("Credits");
                }
                break;
        }
        
        m_onHurt.Raise();
    }

    private void CheckFrequency()
    {
        switch (m_player.CorruptionLevel)
        {
            case CorruptionLevel.Level3:
                m_visionFrequency = 30f;
                break;
            case CorruptionLevel.Level4:
                m_visionFrequency = 20f;
                break;
            case CorruptionLevel.Level5:
                m_visionFrequency = 10f;
                break;
        }
    }

    private void PlayRandomSound()
    {
        int r = Random.Range(0, SoundManager.Instance.Noises.Count);
        SoundManager.PlaySFX(SoundManager.Instance.Noises[r]);
    }

    private void SpawnFakeEnemy()
    {
        GameObject vision = m_visionsPool.Get();

        float spawnDistance = 10;
        Vector3 playerPos = transform.position;
        Vector3 playerDirection = transform.forward;
        Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

        vision.transform.position = spawnPos;
        vision.SetActive(true);
    } 

    private void ChangeWorld()
    {
        if (m_LHSettings.color == m_dark)
        {
            m_LHSettings.color = m_light;
        }
        else
        {
            m_LHSettings.color = m_dark;
        }
    }

    IEnumerator Visions()
    {
        while (m_affected)
        {
            if (m_player.CorruptionLevel == CorruptionLevel.Level3)
            {
                PlayRandomSound();
            }
            else if (m_player.CorruptionLevel == CorruptionLevel.Level4)
            {
                int rand = Random.Range(0, 2);
                if (rand == 1)
                {
                    SpawnFakeEnemy();
                }
                else
                {
                    PlayRandomSound();
                }
            }
            else if (m_player.CorruptionLevel == CorruptionLevel.Level5)
            {
                int rand = Random.Range(0, 3);
                if (rand == 0)
                {
                    SpawnFakeEnemy();
                }
                else if (rand == 1)
                {
                    PlayRandomSound();
                }
                else
                {
                    ChangeWorld();
                }
            }
            yield return new WaitForSeconds(m_visionFrequency);
        }
        
    }
    
}
public enum CorruptionLevel
{
    NotCorrupted, Level1, Level2, Level3, Level4, Level5, FullyCorrupted
}